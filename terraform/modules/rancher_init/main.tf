terraform {
  required_version = ">=1.0.8"

  required_providers {
    helm = {
      source  = "hashicorp/helm"
      version = "2.3.0"
    }

    rancher2 = {
      source  = "rancher/rancher2"
      version = "1.20.1"
    }
  }
}

provider "helm" {
  kubernetes {
    config_path = var.kubeconfig_path
  }
}

provider "rancher2" {
  #alias     = "bootstrap"
  api_url   = "https://admin.rancher.${var.project_domain}"
  bootstrap = true
  insecure  = true
}

provider "rancher2" {
  alias     = "admin"
  api_url   = "https://admin.rancher.${var.project_domain}"
  token_key = rancher2_bootstrap.setup_admin.token
  insecure  = true
}
