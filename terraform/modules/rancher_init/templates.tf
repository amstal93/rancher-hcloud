resource "rancher2_node_driver" "hetzner_node_driver" {
  provider = rancher2.admin

  active  = true
  builtin = false

  name              = "Hetzner"
  ui_url            = "https://storage.googleapis.com/hcloud-rancher-v2-ui-driver/component.js"
  url               = "https://github.com/JonasProgrammer/docker-machine-driver-hetzner/releases/download/3.4.0/docker-machine-driver-hetzner_3.4.0_linux_amd64.tar.gz"
  whitelist_domains = ["storage.googleapis.com"]
}

resource "rancher2_node_template" "hetzner_create_template" {
  name        = "hetzner-default"
  description = "Default template to acquire Hetzner Cloud Nodes"
  driver_id   = rancher2_node_driver.hetzner_node_driver.id

  hetzner_config {
    api_token       = var.hcloud_token
    image           = "ubuntu-20.04"
    server_type     = "cx31"
    server_location = "nbg1"
    networks        = var.network_name
  }
}
