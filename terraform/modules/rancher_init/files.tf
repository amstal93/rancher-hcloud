resource "local_file" "kube_config_server_yaml" {
  filename = ".kube/config.yaml"
  content  = var.kubeconfig_yaml
}
