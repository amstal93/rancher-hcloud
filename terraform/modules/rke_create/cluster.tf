# Provides RKE cluster resource. This can be used to create RKE clusters and retrieve their information.
# https://registry.terraform.io/providers/rancher/rke/latest/docs/resources/cluster

resource "rke_cluster" "rancher" {
  cluster_name = "rancher"

  dynamic "nodes" {
    for_each = hcloud_server.rancher
    content {
      address           = nodes.value.ipv4_address
      hostname_override = "${nodes.value.name}.rancher.${var.project_domain}"
      internal_address  = hcloud_server_network.rancher[nodes.key].ip
      role              = ["controlplane", "worker", "etcd"]
      user              = "root"
      ssh_agent_auth    = true
      ssh_key           = file(var.hcloud_ssh_key_path)
    }
  }

  upgrade_strategy {
    drain                  = true
    max_unavailable_worker = "10%"
  }

  authentication {
    sans = [hcloud_load_balancer.rancher.ipv4]
  }

  # RKE k8s cluster user addons YAML manifest to be deployed
  addons = <<-EOT
    apiVersion: v1
    kind: Secret
    metadata:
      name: hcloud-csi
      namespace: kube-system
    stringData:
      token: ${var.hcloud_token}
EOT

  # RKE k8s cluster user addons YAML manifest urls or paths to be deployed (list)
  addons_include = ["https://raw.githubusercontent.com/hetznercloud/csi-driver/v1.6.0/deploy/kubernetes/hcloud-csi.yml"]
}
