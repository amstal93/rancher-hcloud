terraform {
  required_version = ">=1.0.8"

  required_providers {
    hcloud = {
      source  = "hetznercloud/hcloud"
      version = "~> 1.32.0"
    }

    hetznerdns = {
      source  = "timohirt/hetznerdns"
      version = "~> 1.2.0"
    }

    rke = {
      source  = "rancher/rke"
      version = "~> 1.3.0"
    }
  }
}

provider "hcloud" {
  token = var.hcloud_token
}

provider "hetznerdns" {
  apitoken = var.dns_token
}
