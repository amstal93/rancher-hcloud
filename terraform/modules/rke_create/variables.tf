variable "hcloud_token" {
  type    = string
  default = null
}

variable "dns_token" {
  type    = string
  default = null
}

variable "hcloud_ssh_key_path" {
  type    = string
  default = "~/.ssh/rancher"
}

variable "project_domain" {
  type    = string
  default = null
}

variable "instance_prefix" {
  type    = string
  default = "manager-node"
}

variable "instance_count" {
  type    = number
  default = 3
}

variable "instance_type" {
  type    = string
  default = "cx31"
}

variable "instance_zones" {
  type    = list(string)
  default = ["nbg1", "fsn1", "hel1"]
}

variable "lb_name" {
  type    = string
  default = "rancher-manage-lb"
}

variable "lb_type" {
  type    = string
  default = "lb11"
}

variable "lb_location" {
  type    = string
  default = "nbg1"
}

variable "network_name" {
  type    = string
  default = "rancher"
}

variable "network_ip_range" {
  type    = string
  default = "172.16.0.0/12"
}

variable "subnet_nodes_ip_range" {
  type    = string
  default = "172.16.1.0/24"
}

variable "network_zone" {
  type    = string
  default = "eu-central"
}

variable "project_name" {
  type    = string
  default = "rancher-hcloud-meetup"
}

variable "os_image" {
  type    = string
  default = "ubuntu-20.04"
}
