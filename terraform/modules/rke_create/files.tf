resource "local_file" "kube_config_server_yaml" {
  filename = ".kube/config.yaml"
  content  = rke_cluster.rancher.kube_config_yaml
}

resource "local_file" "rke_state" {
  filename = ".kube/cluster.rkestate"
  content  = rke_cluster.rancher.rke_state
}

resource "local_file" "rke_cluster_yaml" {
  filename = ".kube/cluster.yml"
  content  = rke_cluster.rancher.rke_cluster_yaml
}
