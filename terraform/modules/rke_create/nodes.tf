# Provides an Hetzner Cloud server resource. This can be used to create, modify, and delete servers:
# https://registry.terraform.io/providers/hetznercloud/hcloud/latest/docs/resources/server

resource "hcloud_server" "rancher" {
  # Create a new Hetzner Cloud Server for each k8s worker node
  count = var.instance_count

  # Name of the server to create (must be unique per project and a valid hostname as per RFC 1123)
  name = "${var.instance_prefix}-${count.index + 1}"

  # The datacenter name to create the server in (using TF modulo operation),
  # see hcloud_datacenters TF data source for a listing of all available data centers
  location = element(var.instance_zones, count.index)

  # Name or ID of the image the server is created from
  image = var.os_image

  # Name of the server type this server should be created with,
  # see hcloud_server_types TF data source for a listing of all available server types
  server_type = var.instance_type

  # Cloud-Init user data to use during server creation, see https://community.hetzner.com/tutorials/basic-cloud-config
  user_data = file("${path.module}/scripts/rancher_management_init.sh")

  # Wait for all installation tasks to finish
  provisioner "remote-exec" {
    inline = ["cloud-init status --wait > /dev/null"]

    connection {
      type        = "ssh"
      user        = "root"
      private_key = file(var.hcloud_ssh_key_path)
      host        = self.ipv4_address
    }
  }

  # Placement Group ID the server added to on creation
  placement_group_id = hcloud_placement_group.rancher.id

  # SSH key IDs or names which should be injected into the server at creation time
  ssh_keys = [
    hcloud_ssh_key.management.id
  ]

  depends_on = [
    hcloud_network.rancher,
    hcloud_network_subnet.rancher,
    hcloud_ssh_key.management,
    hcloud_placement_group.rancher,
  ]

  labels = {
    "node-role"    = "rancher-management",
    "project-name" = var.project_name,
    "builder"      = "terraform",
  }
}

# Provides a Hetzner Cloud Server Network to represent a private network on a server in the Hetzner Cloud:
# https://registry.terraform.io/providers/hetznercloud/hcloud/latest/docs/resources/server_network

resource "hcloud_server_network" "rancher" {
  # ID of the network which should be added to the server
  network_id = hcloud_network.rancher.id

  # Connect each Hetzner Cloud Server to the its private network for worker nodes
  count = var.instance_count

  # ID of the server
  server_id = hcloud_server.rancher[count.index].id
}
