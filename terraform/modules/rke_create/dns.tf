# Provides a Hetzner Cloud Reverse DNS Entry to create, modify and reset reverse dns entries for Hetzner Cloud Floating IPs or servers:
# https://registry.terraform.io/providers/hetznercloud/hcloud/latest/docs/resources/rdns

resource "hcloud_rdns" "rancher" {
  # Create a reverse DNS entry for each k8s worker node
  count = length(hcloud_server.rancher)

  # The server the ip_address belongs to
  server_id = hcloud_server.rancher[count.index].id

  # The IP address that should point to dns_ptr
  ip_address = hcloud_server.rancher[count.index].ipv4_address

  # The DNS address the ip_address should resolve to
  dns_ptr = "${hcloud_server.rancher[count.index].name}.rancher.${var.project_domain}"
}

# Provides details about a Hetzner DNS Zone:
# https://registry.terraform.io/providers/timohirt/hetznerdns/latest/docs/data-sources/hetznerdns_zone

data "hetznerdns_zone" "rancher" {
  # Provides details about a Hetzner DNS Zone with the given name that already exists
  name = var.project_domain
}

# Provides a Hetzner DNS Record resource to create, update and delete DNS Records:
# https://registry.terraform.io/providers/timohirt/hetznerdns/latest/docs/resources/hetznerdns_record

resource "hetznerdns_record" "rancher" {
  # Create a DNS entry for each k8s worker node
  count = var.instance_count

  # Id of the DNS zone to create the record in
  zone_id = data.hetznerdns_zone.rancher.id

  # Name of the DNS record to create
  name = replace(hcloud_rdns.rancher[count.index].dns_ptr, ".${var.project_domain}", "")

  # The value of the record (eg. 192.168.1.1),
  # for TXT records with quoted values the quotes have to be escaped in Terraform
  value = hcloud_rdns.rancher[count.index].ip_address

  # The type of the record
  type = "A"

  # Time to live of this record
  ttl = "300"
}

resource "hetznerdns_record" "rancher-lb" {
  # Id of the DNS zone to create the record in
  zone_id = data.hetznerdns_zone.rancher.id

  # Name of the DNS record to create
  name = "admin.rancher"

  # The value of the record (eg. 192.168.1.1),
  # for TXT records with quoted values the quotes have to be escaped in Terraform
  value = hcloud_load_balancer.rancher.ipv4

  # The type of the record
  type = "A"

  # Time to live of this record
  ttl = "300"
}
